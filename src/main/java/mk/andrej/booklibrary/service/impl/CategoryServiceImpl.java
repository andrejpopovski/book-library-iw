package mk.andrej.booklibrary.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.andrej.booklibrary.domain.Category;
import mk.andrej.booklibrary.infrastructure.exception.ResourseNotFoundException;
import mk.andrej.booklibrary.repository.CategoryRepository;
import mk.andrej.booklibrary.service.GenericService;

@Service
@Slf4j
@Transactional
public class CategoryServiceImpl implements GenericService<Category, Integer> {

	@Autowired
	private CategoryRepository categoryRepo;

	@Override
	public Category findById(Integer id) {
		Category entity = categoryRepo.findById(id).orElseThrow(() -> {
			log.error("Category with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		return entity;
	}

	@Override
	public List<Category> findAll() {
		return categoryRepo.findAll();
	}

	@Override
	public Category create(Category entity) {
		log.debug("Execute create Category with parameters {}", entity);
		Category persistedEntity = categoryRepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Category update(Integer id, Category entity) {
		log.debug("Execute update Category with parameters {}", entity);
		Category persistedEntity = categoryRepo.findById(id).orElseThrow(() -> {
			log.error("Category with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		persistedEntity.setCategoryName(entity.getCategoryName());
		persistedEntity.setId(entity.getId());
		return categoryRepo.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Category with id {}", id);
		categoryRepo.deleteById(id);

	}

}
