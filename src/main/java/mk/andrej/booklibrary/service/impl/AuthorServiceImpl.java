package mk.andrej.booklibrary.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.andrej.booklibrary.domain.Author;
import mk.andrej.booklibrary.infrastructure.exception.ResourseNotFoundException;
import mk.andrej.booklibrary.repository.AuthorRepository;
import mk.andrej.booklibrary.service.GenericService;

@Service
@Slf4j
@Transactional
public class AuthorServiceImpl implements GenericService<Author, Integer> {

	@Autowired
	private AuthorRepository authorRepo;

	@Override
	public Author findById(Integer id) {
		Author entity = authorRepo.findById(id).orElseThrow(() -> {
			log.error("Author with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		return entity;
	}

	@Override
	public List<Author> findAll() {
		return authorRepo.findAll();
	}

	@Override
	public Author create(Author entity) {
		log.debug("Execute create Author with parameters {}", entity);
		Author persistedEntity = authorRepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Author update(Integer id, Author entity) {
		log.debug("Execute update Author with parameters {}", entity);
		Author persistedEntity = authorRepo.findById(id).orElseThrow(() -> {
			log.error("Format with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		persistedEntity.setFirstName(entity.getFirstName());
		persistedEntity.setLastName(entity.getLastName());
		persistedEntity.setId(entity.getId());
		persistedEntity.setListBook(entity.getListBook());
		return authorRepo.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Author with id {}", id);
		authorRepo.deleteById(id);

	}

}
