package mk.andrej.booklibrary.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.andrej.booklibrary.domain.Publisher;
import mk.andrej.booklibrary.infrastructure.exception.ResourseNotFoundException;
import mk.andrej.booklibrary.repository.PublisherRepository;
import mk.andrej.booklibrary.service.GenericService;

@Service
@Slf4j
@Transactional
public class PublisherServiceImpl implements GenericService<Publisher, Integer> {

	@Autowired
	private PublisherRepository publisherRepo;

	@Override
	public Publisher findById(Integer id) {
		Publisher entity = publisherRepo.findById(id).orElseThrow(() -> {
			log.error("Publisher with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		return entity;
	}

	@Override
	public List<Publisher> findAll() {
		log.debug("Execute findAll Publisher");
		return publisherRepo.findAll();
	}

	@Override
	public Publisher create(Publisher entity) {
		log.debug("Execute create Publisher with parameters {}", entity);
		Publisher persistedEntity = publisherRepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Publisher update(Integer id, Publisher entity) {
		log.debug("Execute update Publisher with parameters {}", entity);
		Publisher persistedEntity = publisherRepo.findById(id).orElseThrow(() -> {
			log.error("Error, id of the publisher not found");
			return new ResourseNotFoundException("Resourse not found!");
		});

		// TODO Mapper
		persistedEntity.setId(entity.getId());
		persistedEntity.setPublisherName(entity.getPublisherName());
		persistedEntity.setCountry(entity.getCountry());
		persistedEntity.setYearEstablished(entity.getYearEstablished());
		return publisherRepo.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Publisher with id {}", id);
		publisherRepo.deleteById(id);

	}

}
