package mk.andrej.booklibrary.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.andrej.booklibrary.domain.Book;
import mk.andrej.booklibrary.infrastructure.exception.ResourseNotFoundException;
import mk.andrej.booklibrary.repository.BookRepository;
import mk.andrej.booklibrary.service.GenericService;

@Service
@Slf4j
@Transactional
public class BookServiceImpl implements GenericService<Book, Integer> {

	@Autowired
	private BookRepository bookRepo;

	@Override
	public Book findById(Integer id) {
		Book entity = bookRepo.findById(id).orElseThrow(() -> {
			log.error("Book with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		return entity;
	}

	@Override
	public List<Book> findAll() {
		return bookRepo.findAll();
	}

	@Override
	public Book create(Book entity) {
		log.debug("Execute create Book with parameters {}", entity);
		Book persistedEntity = bookRepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Book update(Integer id, Book entity) {
		log.debug("Execute update Book with parameters {}", entity);
		Book persistedEntity = bookRepo.findById(id).orElseThrow(() -> {
			log.error("Book with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		persistedEntity.setBookName(entity.getBookName());
		persistedEntity.setCategories(entity.getCategories());
		//persistedEntity.setCategory(entity.getCategory());
		persistedEntity.setEdition(entity.getEdition());
		persistedEntity.setFormats(entity.getFormats());
		//persistedEntity.setFormat(entity.getFormat());
		persistedEntity.setId(entity.getId());
		persistedEntity.setListAuthor(entity.getListAuthor());
		persistedEntity.setPages(entity.getPages());
		persistedEntity.setPublisher(entity.getPublisher());
		return persistedEntity;
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Book with id {}", id);
		bookRepo.deleteById(id);

	}

}
