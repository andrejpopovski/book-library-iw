package mk.andrej.booklibrary.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.andrej.booklibrary.domain.Format;
import mk.andrej.booklibrary.infrastructure.exception.ResourseNotFoundException;
import mk.andrej.booklibrary.repository.FormatRepository;
import mk.andrej.booklibrary.service.GenericService;

@Service
@Slf4j
@Transactional
public class FormatServiceImpl implements GenericService<Format, Integer> {

	@Autowired
	private FormatRepository formatRepo;

	@Override
	public Format findById(Integer id) {
		Format entity = formatRepo.findById(id).orElseThrow(() -> {
			log.error("Format with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		return entity;
	}

	@Override
	public List<Format> findAll() {
		return formatRepo.findAll();
	}

	@Override
	public Format create(Format entity) {
		log.debug("Execute create Format with parameters {}", entity);
		Format persistedEntity = formatRepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Format update(Integer id, Format entity) {
		log.debug("Execute update Format with parameters {}", entity);
		Format persistedEntity = formatRepo.findById(id).orElseThrow(() -> {
			log.error("Format with id {} was not found!", id);
			return new ResourseNotFoundException("Resourse not found!");
		});
		persistedEntity.setFormatType(entity.getFormatType());
		persistedEntity.setId(entity.getId());
		return formatRepo.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Format with id {}", id);
		formatRepo.deleteById(id);

	}

}
