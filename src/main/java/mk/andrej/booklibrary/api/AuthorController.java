package mk.andrej.booklibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.andrej.booklibrary.domain.Author;
import mk.andrej.booklibrary.infrastructure.endpoints.Endpoints;
import mk.andrej.booklibrary.service.impl.AuthorServiceImpl;

@RestController
@RequestMapping(Endpoints.AUTHOR)
public class AuthorController {

	@Autowired
	private AuthorServiceImpl service;

	@GetMapping("/{id}")
	public Author findById(@PathVariable(value = "id") Integer id) {
		return service.findById(id);
	}

	@GetMapping
	public List<Author> findAll() {
		return service.findAll();
	}

	@PostMapping
	public Author create(@RequestBody Author entity) {
		return service.create(entity);

	}

	@PutMapping("/{id}")
	public Author update(@PathVariable(value = "id") Integer id, @RequestBody Author entity) {
		return service.update(id, entity);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable(value = "id") Integer id) {
		service.deleteById(id);
	}

}
