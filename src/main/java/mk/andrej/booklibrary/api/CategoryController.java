package mk.andrej.booklibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.andrej.booklibrary.domain.Category;
import mk.andrej.booklibrary.infrastructure.endpoints.Endpoints;
import mk.andrej.booklibrary.service.impl.CategoryServiceImpl;

@RestController
@RequestMapping(Endpoints.CATEGORY)
public class CategoryController {

	@Autowired
	private CategoryServiceImpl service;

	@GetMapping("/{id}")
	public Category findById(@PathVariable(value = "id") Integer id) {
		return service.findById(id);
	}

	@GetMapping
	public List<Category> findAll() {
		return service.findAll();
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Category create(@RequestBody Category entity) {
		return service.create(entity);
	}

	@PutMapping("/{id}")
	public Category update(@PathVariable(value = "id") Integer id, @RequestBody Category entity) {
		return service.update(id, entity);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable(value = "id") Integer id) {
		service.deleteById(id);
	}

}
