package mk.andrej.booklibrary.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.andrej.booklibrary.domain.Book;
import mk.andrej.booklibrary.infrastructure.endpoints.Endpoints;
import mk.andrej.booklibrary.service.impl.BookServiceImpl;

@RestController
@RequestMapping(Endpoints.BOOK)
public class BookController {

	@Autowired
	private BookServiceImpl service;

	@GetMapping("/{id}")
	public Book findById(@PathVariable(value = "id") Integer id) {
		return service.findById(id);
	}

	@GetMapping
	public List<Book> findAll() {
		return service.findAll();
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Book create(@RequestBody Book entity) {
		return service.create(entity);
	}

	@PutMapping("/{id}")
	public Book update(@PathVariable(value = "id") Integer id, @RequestBody Book entity) {
		return service.update(id, entity);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable(value = "id") Integer id) {
		service.deleteById(id);
	}

}
