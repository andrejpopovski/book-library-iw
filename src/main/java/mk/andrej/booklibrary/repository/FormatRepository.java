package mk.andrej.booklibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mk.andrej.booklibrary.domain.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Integer> {

}
