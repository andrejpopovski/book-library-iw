package mk.andrej.booklibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mk.andrej.booklibrary.domain.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
