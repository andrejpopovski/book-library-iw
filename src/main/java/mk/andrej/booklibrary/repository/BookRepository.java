package mk.andrej.booklibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mk.andrej.booklibrary.domain.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

}
