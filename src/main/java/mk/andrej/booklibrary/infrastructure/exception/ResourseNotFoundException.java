package mk.andrej.booklibrary.infrastructure.exception;

public class ResourseNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ResourseNotFoundException() {
		super();
	}

	public ResourseNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourseNotFoundException(String message) {
		super(message);
	}

	public ResourseNotFoundException(Throwable cause) {
		super(cause);
	}

}
