package mk.andrej.booklibrary.infrastructure.endpoints;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Endpoints {
	public static final String BASE = "/library/";
	public static final String CATEGORY = BASE + "category/";
	public static final String FORMAT = BASE + "format/";
	public static final String BOOK = BASE + "book/";
	public static final String AUTHOR = BASE + "author/";
	public static final String PUBLISHER = BASE + "publisher/";

}
