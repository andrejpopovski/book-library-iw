package mk.andrej.booklibrary.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "category")
public class Category {
	@Id
	@SequenceGenerator(name = "category_seq", sequenceName = "category_seq1")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
	private Integer id;
	@Column(name = "category_name", unique = true)
	private String categoryName;

}
