package mk.andrej.booklibrary.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "author")
public class Author {
	@Id
	@SequenceGenerator(name = "author_seq", sequenceName = "author_seq1")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_seq")
	private Integer id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;

	@ManyToMany(mappedBy = "listAuthor", cascade = CascadeType.ALL)
	private List<Book> listBook;

}
