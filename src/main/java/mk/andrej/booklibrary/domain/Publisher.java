package mk.andrej.booklibrary.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "publisher")
public class Publisher {
	@Id
	@SequenceGenerator(name = "publisher_seq", sequenceName = "publisher_seq1")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "publisher_seq")
	private Integer id;
	@Column(name = "name_publisher", unique = true)
	private String publisherName;
	@Column(name = "year_established")
	private Integer yearEstablished;
	@Column(name = "country")
	private String country;

}
