package mk.andrej.booklibrary.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "book")
public class Book {
	@Id
	@SequenceGenerator(name = "book_seq", sequenceName = "book_seq1")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq")
	private Integer id;
	@Column(name = "book_name", nullable = false)
	private String bookName;
	@Column(name = "pages")
	private Integer pages;
	@Column(name = "edition", nullable = false)
	private Integer edition;

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "category_id")
	private List<Category> categories;

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "format_id")
	private List<Format> formats;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "publisher_id")
	private Publisher publisher;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "book_author", joinColumns = @JoinColumn(name = "book_id"), inverseJoinColumns = @JoinColumn(name = "author_id"))
	private List<Author> listAuthor;

}
