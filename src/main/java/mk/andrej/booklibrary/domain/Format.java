package mk.andrej.booklibrary.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "format")
public class Format {
	@Id
	@SequenceGenerator(name = "format_seq", sequenceName = "format_seq1")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "format_seq")
	private Integer id;
	@Column(name = "format_type", unique = true)
	private String formatType;

}
